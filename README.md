# Griffith

A movie collection manager written in Python and GTK3.
---

## Griffith

This document was last updated on Sun Dec 19 2021.
Please see the file COPYING for licensing and warranty information.
The latest version of this software is available at the following URL:
https://gitlab.com/Strit/griffith

### Table of Contents

* Introduction
* System Requirements
* Installation
* Reporting Bugs
* TODO list
* About the Authors

### Introduction

Griffith is a film collection manager, released under the GNU/GPL License.

### System Requirements

| Name           | Minimum version                                                                                   | URL                        | NOTE |
|----------------|---------------------------------------------------------------------------------------------------|----------------------------|------|
| Python         | 3.7 or higher                                                                                     | http://www.python.org/     |      |
| GTK+           | tested on 3.22.10                                                                                 | http://www.gtk.org/        |      |
| python-gobject | 2.6.8                                                                                             | http://www.pygtk.org/      |      |
| SQLAlchemy     | tested on 1.3.23, 1.4.28, 1.4.46 (python 3.8), 1.4.49 (python 3.11), 2.0 is currently not working | http://www.sqlalchemy.org/ |      |
| Pillow         | tested on 4.3.0                                                                                   | https://python-pillow.org/ |      |
| ReportLab      | 3.4.0                                                                                             | http://www.reportlab.org   |      |

**Other (optional) dependencies:**

* TMDB plugin support: tmdbsimple 2.1.0 or higher
* PostgreSQL support: Psycopg2 Tested on 2.7.3.2   http://initd.org/tracker/psycopg/wiki/PsycopgTwo
* MySQL support: PyMySQL Tested on 0.8.0
* Encoding detection of imported CSV file support: chardet                            http://chardet.feedparser.org/
* Covers and reports support: PDF reader
* Spellchecking support: nuspell 3.1.0 or higher     https://nuspell.github.io/

**To check dependencies:**

```
  $ ./griffith --check-dep
```

**To show detected Python modules versions:**

```
  $ ./griffith --show-dep
```

Windows installer includes all the needed requirements.
A GTK+ runtime is not necessary when using this installer.

### External databases

You need to prepare a new database and a new user by yourself

**PostgreSQL**

    CREATE USER griffith UNENCRYPTED PASSWORD 'gRiFiTh' NOCREATEDB NOCREATEUSER;
    CREATE DATABASE griffith WITH OWNER = griffith ENCODING = 'UNICODE';
    GRANT ALL ON DATABASE griffith TO griffith;

**MySQL**

    CREATE DATABASE `griffith` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
    CREATE USER 'griffith'@'localhost' IDENTIFIED BY 'gRiFiTh';
    CREATE USER 'griffith'@'%' IDENTIFIED BY 'gRiFiTh';
    GRANT ALL ON `griffith` . * TO 'griffith'@'localhost';
    GRANT ALL ON `griffith` . * TO 'griffith'@'%';

**Microsoft SQL Server**

    CREATE DATABASE griffith
    EXEC sp_addlogin @loginame='griffith', @passwd='gRiFiTh', @defdb='griffith'
    GO
    USE griffith
    EXEC sp_changedbowner @loginame='griffith'

### Installation

See INSTALL file

### Reporting Bugs

If you want to help or report any bugs founded please visit:
https://gitlab.com/Strit/griffith/issues

### TODO

See TODO file

### About the Authors

See AUTHORS file


---

## Maintenance mode

This project is currently in maintenance mode, since I don't really know Python.

I am happy to accept merge requests for new, tested, features and bugfixes.
